from django.contrib import admin
from recipes.models import Recipe, RecipeStep, Ingredient

# Register your models here.
#admin.site.register(Recipe)


@admin.register(Recipe)
class recipeAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "title",
        "description",
        "created_on"
    ]


@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = [
        "step_number",
        "instruction",
        "id",
    ]


@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    list_display = [
        Recipe,
        "amount",
        "food_item",

    ]
