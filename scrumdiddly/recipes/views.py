from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required
def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        recipe = RecipeForm(request.POST, instance= recipe )
        if recipe.is_valid():
            recipe.save()

            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance = recipe)
    context = {
        "recipe_object": recipe,
        "post_form": form,
    }
    return render(request, "recipes/edit.html", context)

@login_required
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            form.save()
            return redirect('recipe_list')
    form = RecipeForm()
    context = {"form": form}
    return render(request, "recipes/create.html", context)

def show_recipe(request, id):
    recipe = Recipe.objects.get( id = id )
    #or
    recipe = get_object_or_404(Recipe, id = id)
    #context = dictionary of all things you want
    #to show on a template
    context={
        "recipe_object": recipe,
        "cook_name": "Silas"
    }
    return render(request, 'recipes/details.html', context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)

@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipe/list.html", context)
